class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments, id: :uuid do |t|
      t.string :comment, null: false

      t.references :project, foreign_key: true, type: :uuid
      t.references :user, foreign_key: true, type: :uuid
      t.references :parent, foreign_key: { to_table: :comments }, type: :uuid

      t.timestamps
    end
  end
end
