# frozen_string_literal: true

# Controller class for Comments
class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :read_current_comment, only: %i[show]

  def show; end

  def create
    Comment.create(comment: params[:comment], project_id: params[:project_id], user_id: current_user.id)

    redirect_to project_path(id: params[:project_id])
  end

  private

  def read_current_comment
    @comment = Comment.find(params[:id])
  end
end
