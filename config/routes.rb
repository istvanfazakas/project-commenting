# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'sessions' }
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  resources :projects, only: %i[index show update] do
    resources :comments, only: %i[show create]
  end

  # Defines the root path route ("/")
  root 'projects#index'
end
