# frozen_string_literal: true

# Controller for projects
class ProjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :read_current_project, only: %i[show update]

  def index
    @projects = Project.all
  end

  def show; end

  def update
    @project.update(status: params[:status])
  rescue StandardError => e
    flash.alert = e.message
  ensure
    redirect_to project_path(@project)
  end

  private

  def read_current_project
    @project = Project.includes(:comments).find(params[:id])
  end
end
