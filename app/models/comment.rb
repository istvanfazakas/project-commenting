# frozen_string_literal: true

class Comment < ApplicationRecord
  has_many :comments, class_name: 'Comment', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Comment', optional: true
  belongs_to :project
  belongs_to :user
end
