# frozen_string_literal: true

# App Controller
class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :redirect_back

  def redirect_back(error)
    flash.alert = error.message

    redirect_to root_path
  end
end
