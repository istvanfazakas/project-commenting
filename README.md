# Project Commenting

## Installation:

### Database user and password:
- Development:
    - user: dev_db_user
    - password: SomeFancyPasswordH3r3

### Initialize
After DB access is granted, make sure you run:
- `bin/rails db:setup`
- `bin/rails db:migrate`
- `bin/rails db:seed`

### Users:
- You can find the email addresses as following:
    - open rails console: `bin/rails c`
    - run the follwing command: User.pluck(:email)
- Password is `Aloha123` for all the users
