# frozen_string_literal: true

# Sessions controller
class SessionsController < Devise::SessionsController
  before_action :authorize_access_request!, only: :destroy
end
