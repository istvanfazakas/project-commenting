# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :comments, -> { where(parent_id: nil) }
  # has_many :direct_comments, -> { where(parent_id: nil) }, class_name: 'Comment'

  enum :status, %i[draft pending verified rejected]

  validates :status, inclusion: { in: statuses.keys }
end
