# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

require 'faker'

if User.count.zero?
  users = [
    User.create(Faker::Internet.user('username', 'email', 'password').merge(password: 'Aloha123')),
    User.create(Faker::Internet.user('username', 'email', 'password').merge(password: 'Aloha123')),
    User.create(Faker::Internet.user('username', 'email', 'password').merge(password: 'Aloha123'))
  ]

  10.times do
    project = Project.create(name: Faker::Book.title, description: Faker::Lorem.sentence, status: rand(0..2))

    rand(3..10).times do
      user = users.sample
      comment = Comment.create(
        user_id: user.id,
        comment: Faker::Lorem.paragraph_by_chars,
        project_id: project.id
      )

      rand(1..3).times do
        Comment.create(
          user_id: user.id,
          comment: Faker::Lorem.paragraph_by_chars,
          project_id: project.id,
          parent_id: comment.id
        )
      end
    end
  end
end
